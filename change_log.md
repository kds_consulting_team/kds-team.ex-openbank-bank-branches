**0.0.2**
Added option to specify base url.
Added documentation for the extractor.
Changed logging level to info.
Added sample configurations, UI schema and descriptions.

**0.0.1**
First working version of the extractor
Supported endpoints are banks, branches, atms, accounts, transactions and products.