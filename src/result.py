import csv
import json
import logging
import os
from kbc.result import KBCResult, KBCTableDef

BANKS_COLUMNS = ['id', 'short_name', 'full_name',
                 'logo', 'website', 'bank_routings']
BANKS_PK = ['id']
BRANCHES_COLUMNS = ['id', 'bank_id', 'name', 'address_line_1', 'address_line_2', 'address_line_3', 'address_city',
                    'address_county', 'address_state', 'address_postcode', 'address_country_code', 'location_latitude',
                    'location_longitude', 'meta_license_id', 'meta_license_name', 'lobby_monday', 'lobby_tuesday',
                    'lobby_wednesday', 'lobby_thursday', 'lobby_friday', 'lobby_saturday', 'lobby_sunday',
                    'drive_up_monday_opening_time', 'drive_up_monday_closing_time', 'drive_up_tuesday_opening_time',
                    'drive_up_tuesday_closing_time', 'drive_up_wednesday_opening_time',
                    'drive_up_wednesday_closing_time', 'drive_up_thursday_opening_time',
                    'drive_up_thursday_closing_time', 'drive_up_friday_opening_time', 'drive_up_friday_closing_time',
                    'drive_up_saturday_opening_time', 'drive_up_saturday_closing_time', 'drive_up_sunday_opening_time',
                    'drive_up_sunday_closing_time', 'branch_routing_scheme', 'branch_routing_address', 'is_accessible',
                    'accessibleFeatures', 'branch_type', 'more_info', 'phone_number']
BRANCHES_PK = ['id', 'bank_id']
ATMS_COLUMNS = ['id', 'bank_id', 'name', 'address_line_1', 'address_line_2', 'address_line_3', 'address_city',
                'address_county', 'address_state', 'address_postcode', 'address_country_code', 'location_latitude',
                'location_longitude', 'meta_license_id', 'meta_license_name', 'monday_opening_time',
                'monday_closing_time', 'tuesday_opening_time', 'tuesday_closing_time', 'wednesday_opening_time',
                'wednesday_closing_time', 'thursday_opening_time', 'thursday_closing_time', 'friday_opening_time',
                'friday_closing_time', 'saturday_opening_time', 'saturday_closing_time', 'sunday_opening_time',
                'sunday_closing_time', 'is_accessible', 'located_at', 'more_info', 'has_deposit_capability']
ATMS_PK = ['id', 'bank_id']
PRODUCTS_COLUMNS = ['bank_id', 'code', 'parent_product_code', 'name', 'category', 'family', 'super_family',
                    'more_info_url', 'details', 'description', 'meta_license_id', 'meta_license_name']
PRODUCTS_PK = ['code', 'bank_id']
ACCOUNTS_COLUMNS = ['id', 'label', 'bank_id',
                    'account_routings', 'balance_currency', 'balance_amount']
ACCOUNTS_PK = ['id', 'bank_id']
TRANSACTIONS_COLUMNS = ['id', 'bank_id', 'this_account_id', 'this_account_bank_routing_scheme',
                        'this_account_bank_routing_address', 'this_account_account_routings', 'this_account_holders',
                        'other_account_id', 'other_account_holder_name', 'other_account_holder_is_alias',
                        'other_account_bank_routing_scheme', 'other_account_bank_routing_address',
                        'other_account_account_routings', 'details_type', 'details_description', 'details_posted',
                        'details_completed', 'details_new_balance_currency', 'details_new_balance_amount',
                        'details_value_currency', 'details_value_amount']
TRANSACTIONS_PK = ['id', 'bank_id']


class ResultWriter:

    def __init__(self, dataPath):

        self.paramDataPath = dataPath

        self.create_writers()
        self.create_manifests()

    def _create_table_definition(self, tableName, tableColumns, tablePk):

        _fileName = tableName + '.csv'
        _full_path = os.path.join(self.paramDataPath, 'out/tables', _fileName)

        _tbl_def = KBCTableDef(
            name=tableName, columns=tableColumns, pk=tablePk)
        _result_def = KBCResult(file_name=_fileName,
                                full_path=_full_path, table_def=_tbl_def)

        return _result_def

    @staticmethod
    def _create_csv_writer(tableDefinition):

        _writer = csv.DictWriter(open(tableDefinition.full_path, 'w'),
                                 fieldnames=tableDefinition.table_def.columns,
                                 restval='', extrasaction='ignore',
                                 quotechar='"', quoting=csv.QUOTE_ALL)

        return _writer

    def create_writers(self):

        _resultsTableColumn = {'banks': BANKS_COLUMNS,
                               'branches': BRANCHES_COLUMNS,
                               'atms': ATMS_COLUMNS,
                               'products': PRODUCTS_COLUMNS,
                               'accounts': ACCOUNTS_COLUMNS,
                               'transactions': TRANSACTIONS_COLUMNS}

        _resultsPKs = {'banks': BANKS_PK,
                       'branches': BRANCHES_PK,
                       'atms': ATMS_PK,
                       'products': PRODUCTS_PK,
                       'accounts': ACCOUNTS_PK,
                       'transactions': TRANSACTIONS_PK}

        _createdTables = []
        _createdTablesDef = []

        for t in _resultsTableColumn:

            if t not in _createdTables:

                logging.debug("Creating writer for %s." % t)

                _tableDef = self._create_table_definition(
                    t, _resultsTableColumn[t], _resultsPKs[t])
                _writer = self._create_csv_writer(_tableDef)

                if t == 'banks':

                    self.banks = _writer
                    self.banks.writeheader()

                elif t == 'branches':

                    self.branches = _writer
                    self.branches.writeheader()

                elif t == 'atms':

                    self.atms = _writer
                    self.atms.writeheader()

                elif t == 'products':

                    self.products = _writer
                    self.products.writeheader()

                elif t == 'accounts':

                    self.accounts = _writer
                    self.accounts.writeheader()

                elif t == 'transactions':

                    self.transactions = _writer
                    self.transactions.writeheader()

                _writer = None
                _createdTables += [t]
                _createdTablesDef += [_tableDef]

        self.resultTableDefinitions = _createdTablesDef
        self.resultTableNames = _createdTables

    @staticmethod
    def _create_manifest_template(pk=[], incremental=True):

        return {'primary_key': pk, 'incremental': incremental}

    def create_manifests(self):

        for tableDef in self.resultTableDefinitions:

            _manifest = self._create_manifest_template(
                pk=tableDef.table_def.pk)

            _path = tableDef.full_path + '.manifest'
            with open(_path, 'w') as file:

                json.dump(_manifest, file)

    @staticmethod
    def flattenJson(y):
        out = {}

        def flatten(x, name=''):
            if type(x) is dict:
                for a in x:
                    flatten(x[a], name + a + '_')
            else:
                out[name[:-1]] = x

        flatten(y)
        return out
