import os
import logging
import requests
import sys
from kbc.client_base import HttpClientBase


class OpenBankClient(HttpClientBase):

    def __init__(self, username, password, consumerKey, baseUrl):

        self.paramUsername = username
        self.paramPassword = password
        self.paramConsumerKey = consumerKey
        self.paramBaseUrl = baseUrl

        self.getDirectLogin()

        _defHeader = {
            'Authorization': f'DirectLogin token="{self.varApiToken}"',
            'Content-Type': 'application/json'
        }

        super().__init__(self.paramBaseUrl, default_http_header=_defHeader)

    def getDirectLogin(self):

        reqEndpoint = 'my/logins/direct'
        reqUrl = os.path.join(self.paramBaseUrl, reqEndpoint)
        reqAuthString = f'DirectLogin username="{self.paramUsername}", password="{self.paramPassword}", ' + \
                        f'consumer_key="{self.paramConsumerKey}"'

        reqHeaders = {
            'Authorization': reqAuthString,
            'Content-Type': 'application/json'
        }

        reqToken = requests.post(reqUrl, headers=reqHeaders)
        reqSc, reqJs = reqToken.status_code, reqToken.json()

        if reqSc != 201:

            logging.error("Error obtaining the token.")
            logging.error("Received: %s - %s." % (reqSc, reqJs))
            sys.exit(1)

        else:

            self.varApiToken = reqJs['token']

    def getBanks(self):

        reqEndpoint = 'obp/v4.0.0/banks'
        reqUrl = os.path.join(self.base_url, reqEndpoint)

        reqBanks = self.get(reqUrl)
        return reqBanks['banks']

    def getBranches(self, bankId):

        reqEndpoint = f'obp/v4.0.0/banks/{bankId}/branches'
        reqUrl = os.path.join(self.base_url, reqEndpoint)

        limitValue = 100
        offsetValue = 0
        recordsLeft = True

        branches = []

        while recordsLeft is True:

            reqBranches = self.get_raw(reqUrl, params={'limit': limitValue, 'offset': offsetValue})
            reqSc, reqJs = reqBranches.status_code, reqBranches.json()

            if reqSc != 200:

                logging.warn("Could not obtain branches for bank id %s." % bankId)
                logging.debug(reqJs)
                break

            else:

                branches += reqJs['branches']
                offsetValue += limitValue

            if len(reqJs['branches']) < limitValue:

                recordsLeft = False

        return branches

    def getAtms(self, bankId):

        reqEndpoint = f'obp/v4.0.0/banks/{bankId}/atms'
        reqUrl = os.path.join(self.base_url, reqEndpoint)

        limitValue = 100
        offsetValue = 0
        recordsLeft = True

        atms = []

        while recordsLeft is True:

            reqAtms = self.get_raw(reqUrl, params={'limit': limitValue, 'offset': offsetValue})
            reqSc, reqJs = reqAtms.status_code, reqAtms.json()

            if reqSc != 200:

                logging.warn("Could not obtain atms for bank id %s." % bankId)
                logging.debug(reqJs)
                break

            else:

                atms += reqJs['atms']
                offsetValue += limitValue

            if len(reqJs['atms']) < limitValue:

                recordsLeft = False

        return atms

    def getProducts(self, bankId):

        reqEndpoint = f'obp/v4.0.0/banks/{bankId}/products'
        reqUrl = os.path.join(self.base_url, reqEndpoint)

        reqProducts = self.get_raw(reqUrl)
        reqSc, reqJs = reqProducts.status_code, reqProducts.json()

        if reqSc != 200:

            logging.warning("Could not obtain product data for bank id %s." % bankId)
            logging.debug(reqJs)

        else:

            return reqJs['products']

    def getAccounts(self, bankId):

        reqEndpoint = f'obp/v4.0.0/banks/{bankId}/balances'
        reqUrl = os.path.join(self.base_url, reqEndpoint)

        reqAccounts = self.get_raw(reqUrl)
        reqSc, reqJs = reqAccounts.status_code, reqAccounts.json()

        if reqSc != 200:

            logging.warn("Could not obtain accounts for bank id %s." % (bankId))
            logging.debug(reqJs)

        else:

            return reqJs['accounts']

    def getTransactions(self, bankId, accountId):

        reqEndpoint = f'obp/v4.0.0/my/banks/{bankId}/accounts/{accountId}/transactions'
        reqUrl = os.path.join(self.base_url, reqEndpoint)

        allTransactions = []
        recordsLeft = True
        limit = 100
        offset = 0

        while recordsLeft is True:

            reqTransactions = self.get_raw(reqUrl, params={'limit': limit, 'offset': offset})
            reqSc, reqJs = reqTransactions.status_code, reqTransactions.json()

            if reqSc != 200:

                logging.warn("Could not obtain transactions for account id %s, bank id %s" % (accountId, bankId))
                logging.debug(reqJs)

            else:

                allTransactions += reqJs['transactions']

            if len(reqJs['transactions']) < limit:

                recordsLeft = True
                break

            else:

                offset += limit

        return allTransactions
