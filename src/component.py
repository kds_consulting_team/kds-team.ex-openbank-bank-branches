import logging
from client import OpenBankClient
from result import ResultWriter
from kbc.env_handler import KBCEnvHandler


USERNAME_KEY = 'username'
PASSWORD_KEY = '#password'
CONSUMER_KEY = '#consumerKey'
BASEURL_KEY = 'baseUrl'

MANDATORY_PARAMS = [USERNAME_KEY, PASSWORD_KEY, CONSUMER_KEY, BASEURL_KEY]


class Component(KBCEnvHandler):

    def __init__(self):

        super().__init__(MANDATORY_PARAMS)
        self.validate_config(MANDATORY_PARAMS)

        self.paramUsername = self.cfg_params[USERNAME_KEY]
        self.paramPassword = self.cfg_params[PASSWORD_KEY]
        self.paramConsumerKey = self.cfg_params[CONSUMER_KEY]
        self.paramBaseUrl = self.cfg_params[BASEURL_KEY]

        self.client = OpenBankClient(self.paramUsername, self.paramPassword,
                                     self.paramConsumerKey, self.paramBaseUrl)
        self.writer = ResultWriter(self.data_path)

    def processBanks(self):

        logging.info("Downloading data about banks.")

        allBanks = self.client.getBanks()
        self.varBankIds = []

        for bank in allBanks:

            flatBank = ResultWriter.flattenJson(bank)
            self.varBankIds += [flatBank['id']]
            self.writer.banks.writerow(flatBank)

    def processBankData(self):

        self.varBranchIds = []
        allBranches = []
        allAtms = []
        allProducts = []

        for bank in self.varBankIds:

            logging.info("Downloading branches, atms and products for bank id %s." % bank)

            allBranches += self.client.getBranches(bank)
            allAtms += self.client.getAtms(bank)
            allProducts += self.client.getProducts(bank)

        for branch in allBranches:

            flatBranch = self.writer.flattenJson(branch)
            self.varBranchIds += [(flatBranch['bank_id'], flatBranch['id'])]
            self.writer.branches.writerow(flatBranch)

        for atm in allAtms:

            flatAtm = self.writer.flattenJson(atm)
            self.writer.atms.writerow(flatAtm)

        for product in allProducts:

            flatProduct = self.writer.flattenJson(product)
            self.writer.products.writerow(flatProduct)

    def processAccounts(self):

        allAccounts = []
        self.varAccounts = []

        for bank in self.varBankIds:

            logging.info("Downloading accounts for bank %s." % bank)

            allAccounts = self.client.getAccounts(bank)

        for account in allAccounts:

            flattAccount = self.writer.flattenJson(account)
            self.varAccounts += [(bank, flattAccount['id'])]
            self.writer.accounts.writerow(flattAccount)

    def processTransactions(self):

        logging.info("Downloading data about transactions.")

        allTransactions = []

        for bankId, accountId in self.varAccounts:

            allTransactions += self.client.getTransactions(bankId, accountId)

        for transaction in allTransactions:

            flatTransaction = self.writer.flattenJson(transaction)
            self.writer.transactions.writerow(flatTransaction)

    def run(self):

        self.processBanks()
        self.processBankData()
        self.processAccounts()
        self.processTransactions()
