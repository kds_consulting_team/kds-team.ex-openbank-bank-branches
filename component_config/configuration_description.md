A sample of the JSON configuration can be found [in the component's repository](https://bitbucket.org/kds_consulting_team/kds-team.ex-openbank-bank-branches/src/master/component_config/sample-config/config.json). By default, 4 parameters are required to run the extractor.

##### Username (`username`)
Name of the user, who will be used to authenticate the user. The user must have enough privileges to access the data in order to be able to download them. If a user is not registered, they can do so in the API section of the [OpenBank Project](https://api.openbankproject.com/oauth/authorize).

##### Password (`#password`)
A password to the username to authenticate the request. Together with username, the password will be used to authenticate all requests with direct login.

##### Consumer Key (`#consumerKey`)
A consumer key for the application. The key can be obtained by registering the application.

##### Base URL (`baseUrl`)
A base of the url for all API requests. The parameter will differ for all instances of OpenBank project depending on the setup.

##### Endpoints (`endpoints`)
A list of endpoints, which should be extracted. Some endpoints require other endpoints to download all the necessary information. If not specified, these endpoints will be automatically downloaded.

### Output

The component outputs 6 tables, one for each of the covered endpoints. All of the tables have a primary key defined and are loaded incrementally into Keboola Storage. 