# OpenBank extractor for Keboola

This extractor allows to download data from [OpenBank project](https://www.openbankproject.com/) and hence get access to data from various financial institutions. Credentials are needed in order to successfully run the extractor, since most of the requests require authentication. The component utilizes direct login and v4.0.0 of the API.

The component was developed for Citizen's Hackathon and the full API documentation can be found [here](https://apiexplorer.openbankproject.com/).

## Endpoints

The component extracts following information from the API:

- banks - `/obp/v4.0.0/banks`
- branches of a bank - `/obp/v4.0.0/banks/BANK_ID/branches`
- atms of a bank - `/obp/v4.0.0/banks/BANK_ID/atms`
- products of a bank - `/obp/v4.0.0/banks/BANK_ID/products`
- accounts in a bank - `/obp/v4.0.0/banks/BANK_ID/balances`
- transactions for an account - `/obp/v4.0.0/my/banks/BANK_ID/accounts/ACCOUNT_ID/transactions`

All of the available inforamtion is extracted, e.g. all atms/accounts for all banks, all transactions for all accounts in all banks, etc.